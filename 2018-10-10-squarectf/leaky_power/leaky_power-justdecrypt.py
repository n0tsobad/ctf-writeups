#!/usr/bin/env python

import json
from base64 import urlsafe_b64decode, b64decode
from Cryptodome.Cipher import AES
from Cryptodome.Util.Padding import unpad
from itertools import permutations

# The JWE file's Base64-encoded fields must be padded with '=' so that len(field) % 4 == 0.
# Otherwise Python's standard Base64 libs we use below just blow up.
with open(r'instructions_corrected_padding.jwe','r') as f:
    json_input = f.readline()

keys = [
         b'\xd2\xde\xa0\x57\xd1\x14\x5f\x45\x67\x96\x96\x60\x24\xa7\x03\xb2'
]

for key in keys:
    b64 = json.loads(json_input)
    json_k = [ 'iv', 'protected', 'ciphertext', 'tag' ]
    jv = {k:urlsafe_b64decode(b64[k]) for k in json_k}    
    
    # Remove the padding symbols back, otherwise integrity check fails
    jv['protected'] = bytearray(b64['protected'].replace('=', ''), encoding='ascii')
    
    cipher = AES.new(key, AES.MODE_GCM, nonce=jv['iv'])

    cipher.update(jv['protected'])
    plaintext = cipher.decrypt_and_verify(jv['ciphertext'], jv['tag'])
    print('The message was: ')
    print(plaintext.decode('ascii'))
    print('Key: {}'.format(repr(key)))
