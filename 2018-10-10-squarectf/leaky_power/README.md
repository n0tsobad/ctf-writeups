Type
=====

Crypto

Description
===========

C4 is a very advanced AES based defensive system. You are able to
monitor the power lines. Is that enough?

You’re given [three files](https://squarectf.com/2018/leaky_power.tar.gz):

    * powertraces.npy: Measurements (over time) of power consumption of a chip while performing AES encryption
    * plaintexts.npy: Corresponding plaintext inputs that were encrypted
    * instructions.jwe: File encrypted using the same key as plaintexts.npy.

note: The first two files are NumPy arrays.

Solution
========

This one was apparently a power analysis task, but took us some time to solve due to the fact that DPA technique we were trying to use was not producing clear key byte candidates (probably due to small number of traces) plus there was a mistake in the file package posted initially (the flag file was encrypted with a wrong header).

At the end of the day we've found the nice tool for power trace analysis - the [ChipWhisperer Analyzer](https://wiki.newae.com/CW-Analyzer_Tool). It performs the CPA type of analysis and was able to produce the right key candidate after we've managed to create proper project configuration files for it using [this article](https://wiki.newae.com/File_Formats).

![CW-Analyzer with the correct key](cw-analyzer-key.png)

But that wasn't all - the problem with the incorrect header manifested itself until we've noticed the origanizers have reuploaded the task files.

After that we've used the [script](./leaky_power-justdecrypt.py) to decrypt the message contents (but not before we've solved the decryption peculiarity - read on below):

```
CONFIDENTIAL

To disable C4, you will need:
- 6 bits of Dragon Sumac
- 1 nibble of Winter Spice
- 1 byte of Drake Cardamom
- 1 flag with value flag-e2f27bac480a7857de45
- 2 diskfulls of Tundra Chives
- 5 forks

Grind the Dragon Sumac in a cup, making sure you don't break the cup as it's probably a delicate cup. Add a sprinkle of
liquid ice to turn it into a cream-like paste, then add the Winter Spice, first almost everything, then the last tiny
remnants.

Fill a pan with elemental water, add the mixture and cool it down with how cool you are, then bring the mixture
to a boil. Let it cool down to the body temperature of a reptile before adding the Drake Cardamom and Tundra Chives,
all at once of one, then half at a time of the other.

Bring everything back to a boil, turn of the heat, mix with the forks and let everything cool down. If you
touch the liquid and it burns you, it hasn't cooled down enough.

Whisk the mixture heavily to aerate it. Stop when it's frothy.

Drinking the potion will disable C4.

note: A small, but very cold amount is needed for the potion to be effective. Mixing it in a milkshake could work, but
be wary of brain freeze.
```

As an anecdote - strictly speaking we've actually used the [jwcrypto](https://github.com/latchset/jwcrypto) library to decrypt the message first, but due to the fact that was done on-the-go in the IPython CLI, no log left. We had to resort to it as our original script listed above was seemingly not working - and it turned out that with JWE format the `protected` field value is taken for the integrity checking as-is, i.e. Base64-encoded, while all the rest *are* decoded first. Go figure.