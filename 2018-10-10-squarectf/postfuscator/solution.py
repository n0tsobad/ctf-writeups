cipher = "12009367807218646859018292134521568805686127287089876612468382748236461208592688982686121828975882178245515674851882"
salts = "4L0ksa1t"
max_len = len(cipher)

def xorme(index, cipher, offset, out):
	print('xorme', 1, cipher, offset, out)
	if index >= max_len:
		return out
	num = int(cipher[:offset])
	salt = ord(salts[index % 8])
	
	print num, 'xor', salt
	char = chr(num ^ salt)
	if char in "abcdef0123456789":
		out += char
		print out
	else:
		print "going back"
		raise Exception('Unable to use character')

	one = ""
	two = ""
	three = ""
	try:
		one = xorme(index+1, cipher[offset:], 1, out)
	except:
		print 'offset 1 didn\'t work'
		pass

	if cipher[offset] == '0':
		return one

	try:
		two = xorme(index+1, cipher[offset:], 2, out)
	except:
		print 'offset 2 didn\'t work'
		pass

	try:
		three = xorme(index+1, cipher[offset:], 3, out)
	except:
		print 'offset 3 didn\'t work'
		pass

	return max([one, two, three], key=len)


index = 1
out = ""
out = xorme(index, cipher, 3, out)

print out
