# Postfuscator challenge

### Description

> The humans who designed C9 were afraid that the Charvises would disavow their pact.
>
> The humans therefore decided to build an [additional](https://squarectf.com/2018/postfuscator.tar.gz) defense system, C9, using an ancient programming language. A programming language that Charvises didn’t even know is a programming language!
>
> Can you figure out the correct input?

### Initial analysis

As title slightly suggests there is postscript involved in this quest.

> Postscript is a programming language that describes the appearance of a printed page. It was developed by Adobe in 1985 and has become an industry standard for printing and imaging.

We are given `postfuscator.sh` file that takes one line of input data and generates `postfuscator.ps` file. As it turns out almost entire content of mentioned `postscript` file is embedded inside `sh` one. The only difference is that input provided by the user is being placed there as additional line.

Postscript is pretty interesting language. Majority of information about it I found in this [bluebook pdf](https://www-cdf.fnal.gov/offline/PostScript/BLUEBOOK.PDF). When writing postscript code developer is forced to adopt constant stack-oriented mindset. Almost everything that appears in the code is pushed onto the stack. When function identifier occurs in the code, arguments (quantity is different for every function of course) are taken from the stack. Thus, the order of arguments for getinterval function is depicted by following reference page entry:

```pos
array index count	getinterval		subarray
```

Result of function call is then pushed on the stack again.

### Reversing the code

So first of all I tried to reverse two functions declared in challenge file `encrypt_str` and `test_str`. All code after those functions (after 43rd line) is just for printing some shapes on the white background and writing text. Here is pseudocode:

```
encrypt_str(input) {
	buf1 = string o dlugosci 65
	buf2 = "4L0ksa1t"
	
	n = 0
	for(i=0; i<len(input); i++) {
		temp = buf2[n % 8] ^ input[i]
		buf1[n] = temp
		n = n+1
	}
	
	return buf1[0:n]
}

test_str (input) {
	buf = "1712009367807218646859018292134521568805686127287089876612468382748236461208592688982686121828975882178245515674851882" 
	ok = 0
	n = 0
	
	for(i=0; i<len(input); i++) {
		c = str(input[i])
		
		if (c == buf[n:len(c)]) {
			ok = ok+len(c)
		}
		n = n+len(c)
	}
	
	return len(buf) == ok
}

test_str(encrypt_str(input))
```

Most important helper in the process of reversing given code was `stack`

> ### stack
>
> This command prints a copy of the entire stack, starting from the top and working to the bottom.

So by the looking at the code you can spot that challenging part is to find input of chars (in abcdef0123456789 range) that XORed with salt will match given harcoded `buf` variable.

### Solution

```python
cipher = "12009367807218646859018292134521568805686127287089876612468382748236461208592688982686121828975882178245515674851882"
salts = "4L0ksa1t"
max_len = len(cipher)

def xorme(index, cipher, offset, out):
        print('xorme', 1, cipher, offset, out)
        if index >= max_len:
                return out
        num = int(cipher[:offset])
        salt = ord(salts[index % 8])

        print num, 'xor', salt
        char = chr(num ^ salt)
        if char in "abcdef0123456789":
                out += char
                print out
        else:
                print "going back"
                raise Exception('Unable to use character')

        one = ""
        two = ""
        three = ""
        try:
                one = xorme(index+1, cipher[offset:], 1, out)
        except:
                print 'offset 1 didn\'t work'
                pass

        if cipher[offset] == '0':
                return one

        try:
                two = xorme(index+1, cipher[offset:], 2, out)
        except:
                print 'offset 2 didn\'t work'
                pass

        try:
                three = xorme(index+1, cipher[offset:], 3, out)
        except:
                print 'offset 3 didn\'t work'
                pass

        return max([one, two, three], key=len)


index = 1
out = ""
out = xorme(index, cipher, 3, out)

print out
```


