https://squarectf.com/2018/index.html

This one quite quite nice result and task-wise - we've got to the 22nd place and the tasks were interesing overall.

The umbrella story was:

```
 You are the last person on planet Charvis 8HD. Everyone has decided to leave
 because Charvis 9HD is the hipper place to live. As the last person to leave,
 your Captain sent you the following instructions:

"Make sure you enable the Charvis 8HD Defense System (CDS) after taking off in
your spaceship."

However, you misread the instructions and activated the CDS before leaving
the planet. You are now stuck on this planet. Can you figure out how to manually
disable the 10 defense systems (C1 thru C10) which comprise CDS in order to safely
take off?

Thankfully, Charvis 8HD does not have strong dust storms and you will not need
to travel 2,000 miles in a rover.
```