# Gofuscated challenge

### Description

The very first settlers didn’t have a lot of confidence in their hand-assembled C6 system. They therefore built C7, an advanced defense system written in Golang.

Given the [source code](https://squarectf.com/2018/gofuscated.tar.gz), can you find the correct input?

### Initial analysis

We are given jar (so zip archive) with source code of Go program. This challenge required some initial knowledge about Go programming language. My opinion is that it was the only difficulty. Prerequisites / constructs from this language that you need to know before tackling this program are `goroutines` and `channels`.

> A *goroutine* is a lightweight thread of execution.

Technically program written in Go starts pool of threads and utilizes them to run goroutines. Internal libraries handles scheduling and signaling hurdles under the hood. As thread creation is considered time consuming operation it is approach taken to save it. Threads are initialized once (quantity depending on the number of cores or predefined by user) and lives through the program execution time.

> *Channels* are the pipes that connect concurrent goroutines. You can send values into channels from one goroutine and receive those values into another goroutine.

Example on [goroutines](https://gobyexample.com/goroutines).
Example on [channels](https://gobyexample.com/channels).

By looking at the code and at how program behave after start I concluded `compute1` is only for animated juggling person. So I assumed it can be skipped. `done` channel was placed there to inform `compute1` that we correctly went through `compute2`. But after further research I concluded that `compute2` was not needed to analyze, either. Actual checks that are placed between us and flag are `compute3`, `panicIfInvalid`, `compute4` and  `another_helper`. If can give input that pass correctly through these function we compute the flag by using `compute2`.

### Reversing

```go
/**
 * Some other computation.
 */
func compute3(r chan byte) chan byte {
	s := make(chan byte)
	go func() {
		var prev byte = 0
		for v := range r {
			if v != prev {
				s <- v
				prev = v
			}
		}
		close(s)
	}()
	return s
}
```

Above function when used in `main`, rewrites byte from input to output when considered byte and previous byte are not equal. Given that information we need to pass input without two identical bytes sitting besides each other.

```go
/**
 * Another boring function
 */
func panicIfInvalid(s string) {
	if !regexp.MustCompile("^[a-zA-Z0-9]{26}$").MatchString(s) {
		panic("invalid input!")
	}
}
```

This function checks where we have letters and digits in our input only. Easy :)

```go
/**
 * These aren't helpful, right?
 */
func compute4(input string) string {
	rand.Seed(42)
	m := make(map[int]int)
	for len(m) < 26 {
		c := rand.Int()%26 + 'a'
		if _, ok := m[c]; !ok {
			m[c] = len(m) + 'a'
		}
	}
	r := ""
	for _, c := range input {
		r = fmt.Sprintf("%s%c", r, m[int(c)])
	}
	return r
}
```

Above function is mixing function. It prepares letter -> letter matching based on some random `ints`. Seed is the same so every program run uses identical mixing. Then our given input is mixed by following generated dictionary.

```go
/**
 * Last one
 */
func another_helper(input string) (r bool) {
	r = true
	for i, v := range input {
		for j, w := range input {
			r = r && (i > j || v <= w)
		}
	}
	return
}
```

The last check verifies that result of `compute4` is sorted ascending.

I used mixing function code in order to generate the dictionary to see it. After that I only needed to prepare input in such way mixing function will result alphabet.

```
y:l
o:n
j:o
r:r
e:c
l:d
i:i
s:k
b:s
c:t
w:u
m:y
k:z
v:e
a:h
f:j
u:p
q:g
d:q
g:v
t:x
p:w
n:a
x:b
z:f
h:m

abcdefghijklmnopqrstuvwxyz
nxelvzqaifsyhojudrbcwgptmk
```

When placed `nxelvzqaifsyhojudrbcwgptmk` to `compute2`it gives flag `flag-705787f208e6eff63768ae166482125b`
